Role Name
=========

Docker role for Ubuntu

Requirements
------------


Role Variables
--------------

defaults for docker-role

    apt_packages:
        - ca-certificates
        - curl
        - gnupg
        - lsb-release

    apt_packages_state: "latest"

    gpg_key_repo_url: "https://download.docker.com/linux/ubuntu/gpg"

    apt_repos_state: "present"

    docker_repo_url: "deb https://download.docker.com/linux/ubuntu focal stable"

    apt_docker_packages:
    - docker-ce
    - docker-ce-cli
    - containerd.io
    - docker-compose-plugin

    apt_docker_packages_state: "latest"

    docker_users: []

Dependencies
------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: app
      roles:
         - docker

License
-------

BSD

Author Information
------------------

Anna Sheludchenko, ITMO University